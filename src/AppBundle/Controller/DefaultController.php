<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryTranslation;
use AppBundle\Entity\Post;
use AppBundle\Entity\Test;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /**
     * @Route("/create-test-posts")
     *
     * @param Request $request
     * @return Response
     */
    public function createTestPostsAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $repository = $em->getRepository('Gedmo\Translatable\Entity\Translation');

        $post = new Post();

        $post->setTitle('My Default Title');
        $post->setDescription('My Coolest Description');

        $repository->translate($post, 'title', 'de', 'My Title DE')
            ->translate($post, 'description', 'de', 'My Coolest Description DE')
            ->translate($post, 'title', 'ru', 'My Title RU')
            ->translate($post, 'description', 'ru', 'My Coolest Description RU')
        ;

//        $em->persist($post);
//        $em->flush();

        return new Response('Successfully created');
    }

    /**
     * @Route("/find-post-de")
     *
     * @param Request $request
     * @return Response
     */
    public function findPostAction(Request $request)
    {
        $id = $request->query->get('id');

        $em = $this->getDoctrine()->getEntityManager();

        $post = $em->find('AppBundle\Entity\Post', $id);

        $content = var_export($post, true);

        $post->setLocale('de');
        $em->refresh($post);

        $content .= var_export($post, true);

        return new Response(sprintf('<pre>%s</pre>', $content));
    }

    /**
     * @Route("/create-tests")
     *
     * @param Request $request
     * @return Response
     */
    public function createTestTranslationsAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $repository = $em->getRepository('Gedmo\Translatable\Entity\Translation');

        $test = new Test();

        $test->setTitle('Test Title');

        $repository->translate($test, 'title', 'de', 'Test Title DE')
            ->translate($test, 'title', 'ru', 'Test Title RU')
        ;

//        $em->persist($test);
//        $em->flush();

        return new Response('Successfully created 2');
    }

    /**
     * @Route("/find-test-de")
     *
     * @param Request $request
     * @return Response
     */
    public function findTestAction(Request $request)
    {
        $id = $request->query->get('id');

        $em = $this->getDoctrine()->getEntityManager();

        $test = $em->find('AppBundle\Entity\Test', $id);

        $test->setLocale('de');
        $em->refresh($test);

        return new Response(sprintf('<pre>%s</pre>', var_export($test, true)));
    }

    /**
     * @Route("/create-category")
     *
     * @return Response
     */
    public function createCategoriesAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $category = new Category();
        $category->setTitle('Some Title');
        $category->addTranslation(new CategoryTranslation('lt', 'title', 'Some Title IT'));

        $em->persist($category);
        $em->flush();

        return new Response('Successfully created 3');
    }
}
