<?php

namespace AppBundle\Entity;

use Gedmo\Translatable\Entity\MappedSuperclass\AbstractTranslation;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="test_translations", indexes={
 *      @ORM\Index(name="test_translation_idx", columns={"locale", "object_class", "field", "foreign_key"})
 * })
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 */
class TestTranslation extends AbstractTranslation
{

}